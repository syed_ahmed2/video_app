import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'pj2f6d3r',
    dataset: 'production'
  }
})
